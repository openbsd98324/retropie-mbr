
# 8gb sdmmc

/dev/mmcblk0p2      499712 15523839 15024128  7.2G 83 Linux

````
Disk /dev/mmcblk0: 7.4 GiB, 7948206080 bytes, 15523840 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x6397c987

Device         Boot  Start      End  Sectors  Size Id Type
/dev/mmcblk0p1 *      8192   499711   491520  240M  e W95 FAT16 (LBA)
/dev/mmcblk0p2      499712 15523839 15024128  7.2G 83 Linux
````





# TDK 8GB  (it is smaller!)

````
Disk /dev/sda: 7.2 GiB, 7739768832 bytes, 15116736 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x6397c987

Device     Boot  Start      End  Sectors  Size Id Type
/dev/sda1  *      8192   499711   491520  240M  e W95 FAT16 (LBA)
/dev/sda2       499712 15523839 15024128  7.2G 83 Linux
````


